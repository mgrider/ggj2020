
# [Super Hex Racer Quest](https://globalgamejam.org/2020/games/super-hex-runner-4)

## About

A VR game for the global game jam 2020


## Instructions

1. Open the project for in Unity 2019.3 for the Android build target. (You will need the Android Build Support modules installed.)
2. Go to Build Settings, and click Build. Save your .apk file somewhere you can find it later.
3. Install the .apk onto your Oculus Quest. You can use adb in Terminal, or SideQuest to drag-and-drop. (SideQuest has good instructions if you've never side-loaded an .apk file on your Oculus Quest before.)
4. In the game, you are trying to get as far "south" as possible before the 20 second timer runs out. You use the A button to teleport, but you can only move so far at once. Each type of tile has a different movement limit. Learn the limits and work within them to make more progress.


## Credits

Made by these folks at the IGDATC jam site:

  * Martin Grider - programming and game design

  * August Brown - art and game design

  * Andy Shih - programming and development

With the following tech:

  * Unity 2019.3.0f6
  * Oculus Quest


