﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AbtractPuzzle.GGJ;

public class GameManager : MonoBehaviour
{
	public static GameManager instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}


	public Material dirtMaterial;
	public Material waterMaterial;
	public Material grassMaterial;
	public Material stoneMaterial;
	public Material mudMaterial;

	public AudioSource introAudio;
	public AudioSource gameAudio;
	public AudioSource outroAudio;

	public GameObject hexPrefab;
	public GameObject dirtHexPrefab;

	public HexagonBehavior startHex;

	public HexagonBehavior currentHex;
	public HexagonBehavior possibleNextHexBehavior;

	public Text statusText;
	public Image timerImage;
	public GameObject highScoreIndicator;

	public enum GameState
	{
		BeforeGameStart,
		GameInProgress,
		GameOver,
		Menu
	}
	public GameState currentState = GameState.Menu;
	private double stateTimeDurationBeforeGameStart = 12.0f;
	private double stateTimeMaxGameInProgress = 20.0f;
	private double stateTimeMaxGameOver = 18.0f;

	private DateTime startStateDateTime;
	private TimeSpan stateDuration;
	private double stateDurationSeconds;

	private double teleportCooldownEnd;
	private double teleportCooldownTotal;
	private bool waitingForCooldown = false;

	public void TeleportHappens()
	{
		if (currentState == GameState.Menu)
		{
			if (possibleNextHexBehavior.data.type == HexagonLevelData.HexType.Stone)
			{
				GameSceneManager.LoadGame();
				BeginState(GameState.BeforeGameStart);
			}
			return;
		}
		currentHex = possibleNextHexBehavior;
		teleportCooldownTotal = HexagonBehavior.TimeToWaitAfterTeleportToType(currentHex.data.type);
		teleportCooldownEnd = stateDurationSeconds + teleportCooldownTotal;
		timerImage.fillAmount = 1.0f;
		waitingForCooldown = true;
		possibleNextHexBehavior = null;
	}

	private void Start()
	{
		BeginState(GameState.BeforeGameStart);
	}

	private void Update()
	{
		stateDuration = DateTime.Now - startStateDateTime;
		stateDurationSeconds = stateDuration.TotalSeconds;
		switch(currentState)
		{
			case GameState.BeforeGameStart:
				if (stateDurationSeconds < 6.0f)
				{
				}
				else if (stateDurationSeconds <= stateTimeDurationBeforeGameStart)
				{
					double timeLeft = stateTimeDurationBeforeGameStart - stateDurationSeconds;
					statusText.text = "Press \"A\" to move\n \n ...in: " + timeLeft.ToString("F2");
				}
				else
				{
					BeginState(GameState.GameInProgress);
				}
				break;
			case GameState.GameInProgress:
				if (stateDurationSeconds > stateTimeMaxGameInProgress)
				{
					BeginState(GameState.GameOver);
				}
				else if (waitingForCooldown)
				{
					double percent = 1.0f + (((teleportCooldownEnd - teleportCooldownTotal) - stateDurationSeconds) / teleportCooldownTotal);
					timerImage.fillAmount = (float)percent;
					if (percent <= 0.0f)
					{
						timerImage.fillAmount = 0.0f;
						waitingForCooldown = false;
					}
				}
				break;
			case GameState.GameOver:
				if (stateDurationSeconds > stateTimeMaxGameOver)
				{
					// let's just reload the scene at this point
					//UnityEngine.SceneManagement.SceneManager.LoadScene("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);
					//Application.LoadLevel(Application.loadedLevel);
					// Teleport the user back to the start
					//Application.Quit();
					GameSceneManager.LoadMenu();
					BeginState(GameState.Menu);
				}
				break;
		}
	}

	public bool IsTeleportAllowed()
	{
		if (currentState == GameState.Menu)
		{
			return true;
		}
		return (currentState == GameState.GameInProgress && !waitingForCooldown);
	}

	private void BeginState(GameState newState)
	{
		// set the new state right away
		currentState = newState;
		// then do all the stuff
		switch (newState)
		{
			case GameState.BeforeGameStart:
				statusText.gameObject.SetActive(true);
				timerImage.gameObject.SetActive(false);
				statusText.text = "Get Ready for...\n \nSuper Hex Racer!!!";
				currentHex = startHex;
				break;
			case GameState.GameInProgress:
				statusText.gameObject.SetActive(false);
				timerImage.gameObject.SetActive(true);
				introAudio.Stop();
				gameAudio.Play();
				timerImage.fillAmount = 0.0f;
				waitingForCooldown = false;
				break;
			case GameState.GameOver:
				statusText.gameObject.SetActive(true);
				timerImage.gameObject.SetActive(false);
				gameAudio.Stop();
				outroAudio.Play();
				int highScore = GetHighScoreValue();
				int distance = HexagonBehavior.DistanceFromCoordToCoord(5, -1, currentHex.data.gridX, currentHex.data.gridZ);
				if (distance >= highScore)
				{
					highScore = distance;
					SetHighScoreValue(highScore);
					highScoreIndicator.SetActive(true);
				}
				statusText.text = "Game Over!\nDistance: " + distance + "\nHigh Score: " + highScore;
				break;
		}
		// for all states
		startStateDateTime = DateTime.Now;
		stateDuration = TimeSpan.FromSeconds(0);
		stateDurationSeconds = 0.0f;
	}


	private string kHighScoreValue = "kHighScoreValue";

	private int GetHighScoreValue()
	{
		if (PlayerPrefs.HasKey(kHighScoreValue))
		{
			return PlayerPrefs.GetInt(kHighScoreValue);
		}
		else
		{
			return 0;
		}
	}

	private void SetHighScoreValue(int newValue)
	{
		PlayerPrefs.SetInt(kHighScoreValue, newValue);
	}
}
