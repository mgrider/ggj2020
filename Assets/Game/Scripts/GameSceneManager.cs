﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AbtractPuzzle.GGJ
{
	public class GameSceneManager : MonoBehaviour
	{
		public static string SceneNameMenu = "Menu";
		public static string SceneNameGame = "Main";

		public static void LoadMenu()
		{
			SceneManager.LoadSceneAsync(SceneNameMenu);
		}

		public static void LoadGame()
		{
			SceneManager.LoadSceneAsync(SceneNameGame);
		}
	}
}

