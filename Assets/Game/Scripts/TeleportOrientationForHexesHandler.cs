﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOrientationForHexesHandler : TeleportOrientationHandler
{
	protected override void InitializeTeleportDestination()
	{
	}

	protected override void UpdateTeleportDestination()
	{
		// todo: let's inject here whether or not the aim is valid...
		//Debug.Log("we are here.")
		LocomotionTeleport.OnUpdateTeleportDestination(AimData.TargetValid, AimData.Destination, null, null);
	}
}

