﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour
{
	[SerializeField]
	private HexagonBehavior a;

	[SerializeField]
	private HexagonBehavior b;

	private void OnEnable()
	{
		int deltaX = Math.Abs(a.data.gridX - b.data.gridX);
		int deltaZ = Math.Abs(a.data.gridZ - b.data.gridZ);


		//int dist = deltaX + deltaZ;
		//if (deltaX > 0 && deltaZ > 0)
		//{
		//	dist--;
		//}
		int dist = HexagonBehavior.DistanceFromCoordToCoord(a.data.gridX, a.data.gridZ, b.data.gridX, b.data.gridZ);

		Debug.Log($"a: x={a.data.gridX}, z={a.data.gridZ}, height={a.data.height}");
		Debug.Log($"b: x={b.data.gridX}, z={b.data.gridZ}, height={b.data.height}");
		Debug.Log($"dist={dist}");
	}

}
