﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HexagonLevelData
{
	public enum HexType
	{
		Dirt,
		Grass,
		Stone,
		Mud,
		Water,
		None,
		Start
	}
	public HexType type = HexType.None;

	public int gridX = -1;
	public int gridZ = -1;

	public int height = 1;

	public HexagonLevelData(int _x, int _z, int _height, string _type)
	{
		this.gridX = _x;
		this.gridZ = _z;
		this.height = _height;
		this.type = HexagonLevelData.ParseType(_type);
	}

	public static HexagonLevelData.HexType ParseType(string pType)
	{
		switch (pType)
		{
			case "D":
				return HexType.Dirt;
			case "G":
				return HexType.Grass;
			case "S":
				return HexType.Stone;
			case "M":
				return HexType.Mud;
			case "W":
				return HexType.Water;
			case "T":
				return HexType.Start;
			default:
				//return HexType.Grass;
				// good enough for a game jam? Naw, let's detect it up stream.
				return HexType.None;
		}
	}

}

public class HexagonBehavior : MonoBehaviour
{
	public HexagonLevelData data;
	public Renderer hexRenderer;

	private bool debugCoordinates = false;

	public bool CanTeleportToHexagonLevelData(HexagonLevelData hexToCompare)
	{
		int heightDiff = Math.Abs(hexToCompare.height - data.height);
		int distance = HexagonBehavior.DistanceFromCoordToCoord(data.gridX, data.gridZ, hexToCompare.gridX, hexToCompare.gridZ);
		switch (data.type)
		{
			case HexagonLevelData.HexType.Grass:
				return (distance <= 2 && heightDiff <= 1);
			case HexagonLevelData.HexType.Dirt:
				return (distance <= 3 && heightDiff <= 1);
			case HexagonLevelData.HexType.Water:
				return (distance <= 1 && heightDiff == 0);
			case HexagonLevelData.HexType.Mud:
				return (distance <= 1 && heightDiff == 0);
			case HexagonLevelData.HexType.Stone:
				if (hexToCompare.height > data.height)
				{
					return (distance == 1 && heightDiff == 1);
				}
				else
				{
					// going same level or lower, can travel FIVE spaces!
					return (distance <= 5);
				}
			case HexagonLevelData.HexType.Start:
				// can only move one space form start
				return (distance == 1);
		}
		return false;
	}

	public static int DistanceFromCoordToCoord(int x1, int z1, int x2, int z2)
	{
		// algorithm found here: http://ondras.github.io/rot.js/manual/#hex/indexing (#2: Odd shift)
		// also: https://www.redblobgames.com/grids/hexagons/
		int penalty = ((isEven(z1) && !isEven(z2) && (x1 < x2)) || (isEven(z2) && !isEven(z1) && (x2 < x1))) ? 1 : 0;
		int dx = x1 - x2;
		int dz = z1 - z2;
		int distance = (int)Math.Max(Math.Abs(dz), Math.Abs(dx) + Math.Floor(Math.Abs(dz) / 2.0f) + penalty);
		return distance;
	}

	private static bool isEven(int x)
	{
		return (x % 2 == 0);
	}

	public static float TimeToWaitAfterTeleportToType(HexagonLevelData.HexType type)
	{
		switch (type)
		{
			case HexagonLevelData.HexType.Grass:
				return 0.5f;
			case HexagonLevelData.HexType.Dirt:
				return 1.0f;
			case HexagonLevelData.HexType.Water:
				return 2.0f;
			case HexagonLevelData.HexType.Mud:
				return 1.5f;
			case HexagonLevelData.HexType.Stone:
				return 1.0f;
			default:
				return 5.0f;
		}
	}

	//void Start()
	//{

	//}

	//void Update()
	//{

	//}

#if UNITY_EDITOR
	public void OnDrawGizmos()
	{
		if (!debugCoordinates) { return; }
		UnityEditor.Handles.BeginGUI();
		Color tempcolor = UnityEditor.Handles.color;
		GUI.color = Color.red;
		UnityEditor.Handles.Label(transform.position, "x:" + data.gridX + ",z:" + data.gridZ);
		GUI.color = tempcolor;
		UnityEditor.Handles.EndGUI();
	}
#endif
}
