﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
	[SerializeField]
	private TextAsset mapFile;

	[SerializeField]
	List<HexagonLevelData> levelHexagons = new List<HexagonLevelData>();

	// this is a hack to get these assigned correctly on the game manager
	public AudioSource introAudio;
	public AudioSource gameAudio;
	public AudioSource outroAudio;
	public HexagonBehavior startHex;
	public Text statusText;
	public Image timerImage;
	public GameObject highScoreIndicator;

	private void SetupGameManager()
	{
		GameManager.instance.introAudio = introAudio;
		GameManager.instance.gameAudio = gameAudio;
		GameManager.instance.outroAudio = outroAudio;
		GameManager.instance.startHex = startHex;
		GameManager.instance.currentHex = startHex;
		GameManager.instance.statusText = statusText;
		GameManager.instance.timerImage = timerImage;
		GameManager.instance.highScoreIndicator = highScoreIndicator;
	}

	private void LoadMapFromFile()
	{
		int x = 0;
		int z = 0;

		string mapData = mapFile.text;
		string[] rows = mapData.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
		foreach (string row in rows)
		{
			string[] tiles = row.Split(',');
			foreach (string tile in tiles)
			{
				if (tile == "")
				{
					continue;
				}
				string[] attributes = tile.Split(' ');
				int height = Int32.Parse(attributes[0]);
				string type = attributes[1];

				// for some reason, the last element of each row is always the wrong type...
				// tried this, didn't help...
				//string type = $"{attributes[1]}";

				//Debug.Log($"z={z}, x={x}, height={height}, type={type}");
				var levelData = new HexagonLevelData(x, z, height, type);
				if (levelData.type == HexagonLevelData.HexType.None)
				{
					// do nothing.
					//HexagonLevelData.HexType wtfType = HexagonLevelData.ParseType(type);
					//Debug.Log("WTF? type " + wtfType + " from char " + type + " for x,z: " + x + "," + z);
				}
				else
				{
					levelHexagons.Add(levelData);
				}
				x++;
			}
			z++;
			x = 0;
		}

	}

	private float hexWidth = 1.74f;
	private float hexHeight = 1.5f;

	private void Start()
	{
		LoadMapFromFile();

		// loop through levelHexagons, instantiate at x/y

		foreach (HexagonLevelData levelHexagon in levelHexagons)
		{
			SetupGameManager();

			GameObject newHex = Instantiate(GameManager.instance.hexPrefab, transform);
			float x = (float)levelHexagon.gridX * hexWidth;
			if (levelHexagon.gridZ % 2 != 0)
			{
				x += (hexWidth / 2.0f);
			}
			float z = (float)levelHexagon.gridZ * hexHeight * -1f;
			Vector3 pos = new Vector3(x, levelHexagon.height, z);
			newHex.transform.position = pos;
			// add dirt hexes underneath as necessary
			for (int y = (levelHexagon.height - 1); y >= 1; y--)
			{
				GameObject dirtHex = Instantiate(GameManager.instance.dirtHexPrefab, transform);
				dirtHex.transform.position = new Vector3(x, y, z);
			}
			// material
			HexagonBehavior hb = newHex.GetComponent<HexagonBehavior>();
			switch (levelHexagon.type)
			{
				case HexagonLevelData.HexType.Grass:
					hb.hexRenderer.material = GameManager.instance.grassMaterial;
					break;
				case HexagonLevelData.HexType.Dirt:
					hb.hexRenderer.material = GameManager.instance.dirtMaterial;
					break;
				case HexagonLevelData.HexType.Stone:
					hb.hexRenderer.material = GameManager.instance.stoneMaterial;
					break;
				case HexagonLevelData.HexType.Water:
					hb.hexRenderer.material = GameManager.instance.waterMaterial;
					break;
				case HexagonLevelData.HexType.Mud:
					hb.hexRenderer.material = GameManager.instance.mudMaterial;
					break;
				default:
					Debug.Log("unhandled levelHexagon.type in matterial assignment: " + levelHexagon.type);
					break;
			}
			// data
			hb.data = levelHexagon;
		}
	}
}
